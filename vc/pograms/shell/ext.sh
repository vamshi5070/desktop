#!/usr/bin/env bash
set -euo pipefail

dir=$1


echo "directory is $dir"
file=$dir/xmenu.sh
prevDir=$(dirname $dir)
icons="IMG:/home/vamshi/vamshiCreations/icons"
extension()
{
    FILE=$1
    ext="${FILE#*.}"
    echo $ext
    filename="${FILE%%.*}"
    case $ext in

        pdf)
            printf "\n$icons/pdf.png\t$filename\tokular $dir/$1" >>$file
            ;;

        hs|py|sh)
            printf "\n$filename\temacs $dir/$1" >>$file
            ;;

        *)
            echo "not found"
            ;;
    esac
}

case $dir in
    /home/vamshi)
        printf "#!/bin/sh\nxmenu -p 12x12:0 <<EOF | sh &">$file
        ;;
    *)
        printf "#!/bin/sh\nxmenu -p 12x12:0 <<EOF | sh &\n..\t$prevDir/xmenu.sh">$file
        ;;
esac
chmod +x $file

elements=$(ls $dir)

for i in $elements
do
    if [ -d $i ]
    then
        printf "\n$icons/directory.png\t$i\t$dir$i/xmenu.sh" >>$file
    else
        extension $i
    fi
done

printf "\nEOF">>$file
