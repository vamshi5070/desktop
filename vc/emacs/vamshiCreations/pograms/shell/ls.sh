#!/usr/bin/env bash
set -euo pipefail

dir=$(pwd)

# elements=$(ls)

app=okular
file=$dir/xmenu.sh

printf "#!/bin/sh\nxmenu <<EOF | sh &\nalacritty\talacritty\n">$file
chmod a+x $file
cnt=0
myFunc()
{

elements=$(ls $1)

for i in $elements
do
    # for $i in `seq 0 $2`;
    # do
    # done
    COUNTER=0
    while [  $COUNTER -lt $2 ]; do
        # echo The counter is $COUNTER
        printf "\t" >> $file
        let COUNTER=COUNTER+1
    done
    printf "$i\n"
    if [ -d $i ]
    then
        printf "$i\n" >> $file
        myFunc $1/$i $(($2+1))
    else
        if [[ $i == "*.pdf"  ]]
        then
            printf "$i\t$app $1/$i\n" >> $file
        else
            continue
        fi
    fi
done

}
myFunc $dir $cnt

echo "EOF">>$file


# if [ -d $dir/$i ]
# then
#     cd $i ; elements=$(ls)
