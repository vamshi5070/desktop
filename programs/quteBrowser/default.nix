{ pkgs, config, lib, inputs, ... }:{
  programs.qutebrowser = {
    enable= true;
    extraConfig = "
# Bindings for cycling through CSS stylesheets from Solarized Everything CSS:
# https://github.com/alphapapa/solarized-everything-css
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save'}
config.set(\"colors.webpage.darkmode.enabled\", True)
c.downloads.location.directory = '~/downloads'
c.url.default_page = 'file://~/home-manager/programs/quteBrowser/homepage.html'
c.url.searchengines = {'DEFAULT': 'https://google.com/search?q={}', 'am': 'https://www.amazon.com/s?k={}', 'aw': 'https://wiki.archlinux.org/?search={}', 'goog': 'https://www.google.com/search?q={}', 'hoog': 'https://hoogle.haskell.org/?hoogle={}', 're': 'https://www.reddit.com/r/{}', 'ub': 'https://www.urbandictionary.com/define.php?term={}', 'wiki': 'https://en.wikipedia.org/wiki/{}', 'yt': 'https://www.youtube.com/results?search_query={}'}
c.colors.completion.fg = ['#9cc4ff', 'white', 'white']
c.colors.completion.odd.bg = '#1c1f24'
c.colors.completion.even.bg = '#232429'
c.colors.completion.category.fg = '#e1acff'
c.colors.completion.category.bg = 'qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #000000, stop:1 #232429)'
c.colors.completion.category.border.top = '#3f4147'
c.colors.completion.category.border.bottom = '#3f4147'
config.bind(',ap', 'config-cycle content.user_stylesheets ~/home-manager/programs/quteBrowser/solarized-everything-css/css/apprentice/apprentice-all-sites.css \"\"')
c.colors.completion.item.selected.fg = '#282c34'
c.colors.completion.item.selected.bg = '#ecbe7b'
c.colors.completion.item.selected.match.fg = '#c678dd'
config.bind(',dr', 'config-cycle content.user_stylesheets ~/home-manager/programs/quteBrowser/solarized-everything-css/css/darculized/darculized-all-sites.css \"\"')
config.bind(',gr', 'config-cycle content.user_stylesheets ~/home-manager/programs/quteBrowser/solarized-everything-css/css/gruvbox/gruvbox-all-sites.css \"\"')
config.bind(',sd', 'config-cycle content.user_stylesheets ~/home-manager/programs/quteBrowser/solarized-everything-css/css/solarized-dark/solarized-dark-all-sites.css \"\"')
config.bind(',sl', 'config-cycle content.user_stylesheets ~/home-manager/programs/quteBrowser/solarized-everything-css/css/solarized-light/solarized-light-all-sites.css \"\"')
config.bind('M', 'hint links spawn mpv {hint-url}')
c.colors.completion.match.fg = '#c678dd'
# Color of the scrollbar handle in the completion view.
# Type: QssColor
c.colors.completion.scrollbar.fg = 'white'

# Background color for the download bar.
# Type: QssColor
c.colors.downloads.bar.bg = '#282c34'

# Background color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.bg = '#ff6c6b'

# Font color for hints.
# Type: QssColor
c.colors.hints.fg = '#282c34'

# Font color for the matched part of hints.
# Type: QtColor
c.colors.hints.match.fg = '#98be65'

# Background color of an info message.
# Type: QssColor
c.colors.messages.info.bg = '#282c34'

# Background color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.bg = '#282c34'

# Foreground color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.fg = 'white'

# Background color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.bg = '#497920'

# Background color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.bg = '#34426f'

# Background color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.bg = '#282c34'

# Foreground color of the URL in the statusbar when there's a warning.
# Type: QssColor
c.colors.statusbar.url.warn.fg = 'yellow'

# Background color of the tab bar.
# Type: QssColor
c.colors.tabs.bar.bg = '#1c1f34'

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = '#282c34'

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = '#282c34'

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = '#282c34'

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = '#282c34'

# Background color of pinned unselected odd tabs.
# Type: QtColor
c.colors.tabs.pinned.odd.bg = 'seagreen'

# Background color of pinned unselected even tabs.
# Type: QtColor
c.colors.tabs.pinned.even.bg = 'darkseagreen'

# Background color of pinned selected odd tabs.
# Type: QtColor
c.colors.tabs.pinned.selected.odd.bg = '#282c34'

# Background color of pinned selected even tabs.
# Type: QtColor
c.colors.tabs.pinned.selected.even.bg = '#282c34'

# Default font families to use. Whenever \"default_family\" is used in a
# font setting, it's replaced with the fonts listed here. If set to an
# empty value, a system-specific monospace default is used.
# Type: List of Font, or Font
c.fonts.default_family = '\"SauceCodePro Nerd Font\"'

# Default font size to use. Whenever \"default_size\" is used in a font
# setting, it's replaced with the size listed here. Valid values are
# either a float value with a \"pt\" suffix, or an integer value with a
# \"px\" suffix.
# Type: String
c.fonts.default_size = '11pt'

# Font used in the completion widget.
# Type: Font
c.fonts.completion.entry = '11pt \"SauceCodePro Nerd Font\"'

# Font used for the debugging console.
# Type: Font
c.fonts.debug_console = '11pt \"SauceCodePro Nerd Font\"'

# Font used for prompts.
# Type: Font
c.fonts.prompts = 'default_size sans-serif'

# Font used in the statusbar.
# Type: Font
c.fonts.statusbar = '11pt \"SauceCodePro Nerd Font\"'


config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')

";
  };
}
