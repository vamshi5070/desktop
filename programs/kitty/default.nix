{ pkgs, config, lib, inputs, ... }:{

programs.kitty = {
    						enable = true;
                        			settings = {
                                        			# font_family        = "Source Code Pro Semibold";
                                        			font_family        = "mononoki Nerd Font mono";
                                        			bold_font          = "auto";
                                        			italic_font        = "auto";
                                        			bold_italic_font   = "auto";
                                        			font_size          = "22.0";
                                        			background_opacity = "1.0";
								                              include            = "~/home-manager/programs/kitty/themes/Dracula.conf";
								#include            = "./Borland.conf";
                        };
  };

}
