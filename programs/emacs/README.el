(setq user-full-name "Vamshi krishna")
(setq user-mail-address "vamshi5070k@gmail.com")

(setq inhibit-startup-message t)

(scroll-bar-mode -1)			
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)

(column-number-mode)

(load-theme 'modus-vivendi t)
 (setq modus-vivendi-theme-distinct-org-blocks t
        modus-vivendi-theme-rainbow-headings t
        modus-vivendi-theme-slanted-constructs t
        modus-vivendi-theme-bold-constructs t
        modus-vivendi-theme-scale-headings t
        modus-vivendi-theme-scale-1 1.05
        modus-vivendi-theme-scale-2 1.1
        modus-vivendi-theme-scale-3 1.15
        modus-vivendi-theme-scale-4 1.2)

(setq initial-scratch-message ">> ")

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

(setq mode-line-format nil)
(setq mode-line-format  '("%e"
			    mode-line-position
			  "    "
			  mode-line-modified
			  "%b"
			  mode-line-remote
			  "    "
			  ;;evil-mode-line-tag
			  "                                       "
			    mode-line-misc-info
			  ))

    (setq modus-themes-mode-line '3d)   
      (display-battery-mode)
    ;; (setq display-time-day-and-date t)
      (display-time-mode)


  (straight-use-package 'hide-mode-line)

(straight-use-package 'use-package)

;; (use-package moody
;;   :straight t
;;   :config
;;   (setq x-underline-at-descent-line t)
;;   (moody-replace-mode-line-buffer-identification)
;;   (moody-replace-vc-mode))

;; (use-package minions
;;   :straight t
;;   ;; :hook (doom-modeline-mode . minions-mode)
;;   :hook (minions-mode)
;;   :custom
;;   (minions-mode-line-lighter ""))

;; These keys should always pass through to Emacs
 (setq exwm-input-prefix-keys
   '(?\C-x
     ?\C-u
     ?\C-h
     ?\M-x
     ?\M-`
     ?\M-&
     ?\M-:
     ?\C-\M-j ;; Buffer list
     ?\C-\ ))  ;; Ctrl+Space

;; (setq exwm-input-global-keys '(
;;  			      ([?\s-return] . vterm)
;;  			    ))

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; zoom in/out like we do everywhere else.
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(defun vc/evil-hook ()
  (dolist (mode '(custom-mode
                  eshell-mode
                  git-rebase-mode
                  erc-mode
                  circe-server-mode
                  circe-chat-mode
                  circe-query-mode
                  sauron-mode
                  term-mode))
  (add-to-list 'evil-emacs-state-modes mode)))

(use-package evil
  :straight t
  :init
  (setq evil-want-keybinding nil)
  (setq evil-want-integration t)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  (setq evil-respect-visual-line-mode t)
  ;;(setq evil-undo-system 'undo-tree)
  :config
  (add-hook 'evil-mode-hook 'vc/evil-hook)
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

;;   ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line))

(use-package evil-collection
  :straight t
  :after evil
  :init
  (setq evil-collection-company-use-tng nil)  ;; Is this a bug in ;;evil-collection?
  :custom
  (evil-collection-outline-bind-tab-p nil)
  :config
  (setq evil-collection-mode-list
        (remove 'lispy evil-collection-mode-list))
   (evil-collection-init))

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

(set-face-attribute 'default nil
		    :font "FiraCode Nerd Font Mono"
		    :height 180
		    :weight 'regular)
      (set-face-attribute 'variable-pitch nil
			  :font "FiraCode Nerd Font Mono"
	   :height 120
	   :weight 'regular)
      (set-face-attribute 'fixed-pitch nil
	   :font "FiraCode Nerd Font Mono"
	   :height 180
	   :weight 'regular)
      (set-face-attribute 'font-lock-comment-face nil
	   :slant 'italic)
      (set-face-attribute 'font-lock-keyword-face nil
	   :slant 'italic)

    ;; (set-face-attribute 'mode-line nil
    ;;                     :font "Ubuntu Mono"
    ;;                     :height 238
    ;;                     :weight 'bold)

    ;; (set-face-attribute 'mode-line-inactive nil
    ;;                     :font "Ubuntu Mono"
    ;;                     :height 238
    ;;                     :weight 'bold)

(straight-use-package 'selectrum)
(selectrum-mode +1)

(use-package orderless
  :straight t
  :ensure t
  :custom (completion-styles '(orderless)))

(use-package consult
  :straight t)

;; Turn on indentation and auto-fill mode for Org files
(defun vc/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 0)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil)
  (setq org-image-actual-width nil)
  (diminish org-indent-mode))
  ;; (org-superstar-mode 1))

(use-package org
  ;; :straight t
  :defer t
  :hook (org-mode . vc/org-mode-setup)
  )

(use-package org-tempo
  :ensure nil) ;; tell use-package not to try to install org-tempo since it's already there.

(use-package magit
  ;; :bind ("M-;" . magit-status)
  :straight t
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; (vc/leader-key-def
;;   "g"   '(:ignore t :which-key "git")
;;   "gs"  'magit-status
;;   "gd"  'magit-diff-unstaged
;;   "gc"  'magit-branch-or-checkout
;;   "gl"   '(:ignore t :which-key "log")
;;   "glc" 'magit-log-current
;;   "glf" 'magit-log-buffer-file
;;   "gb"  'magit-branch
;;   "gP"  'magit-push-current
;;   "gp"  'magit-pull-branch
;;   "gf"  'magit-fetch
;;   "gF"  'magit-fetch-all
;;   "gr"  'magit-rebase)

(use-package which-key
      :straight t
      :defer 0
      :diminish which-key-mode
      :config
      (which-key-mode)
      (setq which-key-idle-delay 1.0))

(use-package general
  :straight t
  :config
  (general-evil-setup t)

  (general-create-definer vc/leader-key-def
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")

  (general-create-definer vc/ctrl-x-keys
    :prefix "C-x")

  (general-create-definer vc/ctrl-c-keys
    :prefix "C-c"))

(use-package dmenu
  :straight t)
(vc/leader-key-def
   "a"  '(dmenu  :which-key "dmenu" ))

(straight-use-package 'hydra)
 (defhydra main ()
  "comment"
("f" find-file "files")
   )

 ;; (vc/leader-key-def
 ;;            "h"    '(main/body :which-key "file"))

(setq tab-bar-new-tab-choice "*scratch*")

(vc/leader-key-def
     "RET"    '(tab-switcher :which-key "tab switcher"))

(setq tab-bar-show nil)  
;; (setq tab-bar-close-button-show nil
;;        tab-bar-new-button-show nil)
 (vc/ctrl-x-keys
   "w" '(:ignore t :which-key "workspaces")
   "w s" '(tab-bar-switch-to-tab :which-key "switch prompt")
   "w n" '(tab-bar-switch-to-next-tab :which-key "next ")
   "w k" '(tab-bar-close-tab :which-key "kill")
   "w a" '(tab-bar-close-other-tabs :which-key "kill other")
   "w c" '(tab-bar-new-tab-to :which-key "new")
   "w u" '(tab-bar-undo-close-tab :which-key "undo")
   "w p" '(tab-bar-switch-to-prev-tab :which-key "prev"))

(defun reload ()
  (interactive)
   (load-file (expand-file-name "~/.emacs.d/init.el"))
  )
(defun privateConfig ()
  (interactive)
  (find-file (expand-file-name "~/home-manager/programs/emacs/README.org")) 
  )
 (vc/leader-key-def
    "f"  '(:ignore t :which-key "files" )
    ;"t w" 'whitespace-mode
 "f p" '(privateConfig :which-key "edit emacs config")
 "r" '(reload  :which-key "reload emacs config")
   "." '(find-file :which-key "find-file")
   "f f" '(find-file :which-key "find-file")
    "f s" '(save-buffer :which-key "save the world"))

(vc/leader-key-def
   "SPC"  '(execute-extended-command  :which-key "M-x" ))

(vc/leader-key-def
  "b b" '(consult-buffer :which-key "switch buffer")
  "b i" '(ibuffer :which-key "switch buffer")
  "b j" '(switch-to-prev-buffer :which-key "prev buffer")
  "b k" '(switch-to-next-buffer :which-key "next buffer")
  ;; "b q a" '(-to-next-buffer :which-key "kill buffers")
  "b q q" '(kill-this-buffer :which-key "kill buffer"))

(vc/leader-key-def
    "w" '(:ignore t :which-key "windows")
    "w k" '(delete-window :which-key "kill windows")
    "w a" '(delete-other-windows :which-key "kill all but this")
    "w w" '(other-window :which-key "next window")
    "w v" '(split-window-right :which-key "vertical split")
    "w h" '(split-window-below :which-key "horizontal split"))

(vc/leader-key-def
  "c"   '(:ignore t :which-key "code")
  "c m" '(consult-imenu :which-key "imenu")
  "c c" '(comment-line :which-key "comment"))

(defun haskellRepl  ()
  (interactive)
  (progn (split-window-horizontally) (other-window -1) (run-haskell) ));;(switch-to-buffer-other-window "*haskell*")))

(use-package company
  :straight t)
(global-company-mode)

(use-package haskell-mode
    :straight t)

(vc/leader-key-def
  "d" '(:ignore t :which-key "haskell")
  "d r" '(haskellRepl :which-key "repl"))

(use-package nix-mode
  :straight t)

(use-package cider
      :straight t)
  (add-hook 'clojure-mode-hook #'cider-mode)

  (vc/leader-key-def
       "c e" '(cider-eval-last-sexp :which-key "clojure")
       "c q" '(cider-insert-last-sexp-in-repl :which-key "clojure"))
       ;; cider-insert-last-sexp-in-repl)

(add-hook 'cider-repl-mode-hook #'cider-company-enable-fuzzy-completion)
(add-hook 'cider-mode-hook #'cider-company-enable-fuzzy-completion)

;;(async-shell-command "brightnessctl s 1")
(defun shutdown ()
  (interactive)
  (async-shell-command "poweroff"))
(defun restart ()
  (interactive)
  (async-shell-command "reboot"))
(defun bluetooth ()
  (interactive)
  (async-shell-command "pavucontrol ; kitty -e bluetoothctl"))
(defun redshiftStop ()
  (interactive)
  (async-shell-command "systemctl --user stop redshift.service"))
(defun redshiftRestart ()
  (interactive)
  (async-shell-command "systemctl --user restart redshift.service"))
(vc/leader-key-def
    "l" '(:ignore t :which-key "linux")
    "l e" '(:ignore t :which-key "exit")
    "l e s" '(shutdown :which-key "shutdown")
    "l e r" '(restart :which-key "restart")
    "l r" '(:ignore t :which-key "redshift")
    "l s" '(:ignore t :which-key "screenshot")
    "l r s" '(redshiftStop :which-key "redshift stop")
    "l r r" '(redshiftRestart :which-key "redshift restart")
    "l b" '(bluetooth :which-key "bluetooth"))

(defun mpv-play-url (url &rest args)
  ""
  (interactive)
  (start-process "mpv" nil "mpv" url))

(setq browse-url-browser-function
  (quote
    (("youtu\\.?be" . mpv-play-url)
    ("." . eww-browse-url))))

(use-package pdf-tools
     :defer t
     :commands (pdf-view-mode pdf-tools-install)
     :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
     :magic ("%PDF" . pdf-view-mode)
     :config
     (pdf-tools-install)
     (define-pdf-cache-function pagelabels)
     :hook ((pdf-view-mode-hook . (lambda () (display-line-numbers-mode -1)))
            (pdf-view-mode-hook . pdf-tools-enable-minor-modes)))
(add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)

(use-package writeroom-mode
  :straight t)

(vc/leader-key-def
      "h" '(:ignore t :which-key "help")
      "h k" '(describe-key :which-key "key")
      "h v" '(describe-variable :which-key "variable")
      "h c" '(describe-command :which-key "commands")
  )
