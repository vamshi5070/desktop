{ config,pkgs,sources, ... }:

let

  nurNoPkgs = import (import ../../nix/sources.nix).nur { };
  # nurNoPkgs.repos.rycee = import ../../nur-expressions { pkgs = throw "nixpkgs eval"; };

  pcfg = config.programs.emacs.init.usePackage;
in
{
 imports = [
    nurNoPkgs.repos.rycee.hmModules.emacs-init
    nurNoPkgs.repos.rycee.hmModules.emacs-notmuch
  ];

  nixpkgs.overlays = [ (import sources.emacs-overlay) ];

    programs.emacs.init = {
    	enable = true;
    };
}
