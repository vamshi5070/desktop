{ config, lib, pkgs, ... }:
let
    haskell-env = pkgs.haskellPackages.ghcWithHoogle (
  hp: with hp; [
  hlint
 ]
);

in
{
  home.packages = with pkgs; [
    haskell-env
  ];
}

