
{ pkgs, ... }:

let

  my-emacs = pkgs.emacsWithPackagesFromUsePackage {
    config = "~/.emacs.d/init.el";
    package = pkgs.emacsGcc;
    alwaysTangle = true;
    extraEmacsPackages = epkgs: [
      epkgs.exwm
      epkgs.emacsql-sqlite
      epkgs.vterm
      epkgs.pdf-tools
    ];
  };
  exwm-load-script = pkgs.writeText "exwm-load.el" ''
    (progn
      (require 'exwm)
      ;;(configure-ivy-posframe-for-exwm)
      ;;(require 'exwm-config)
      ;;(exwm-config-default))
      (exwm-enable)
      (require 'exwm-randr)
      (setq exwm-randr-workspace-monitor-plist '(0 "eDP-1"))
      (add-hook 'exwm-randr-screen-change-hook
             (lambda ()
               (start-process-shell-command
                "xrandr" nil "xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal")))
      (exwm-randr-enable)
      )
  '';
in {
  xsession = {
    enable = true;
    windowManager.command = ''
          ${my-emacs}/bin/emacs -l "${exwm-load-script}"
         '';
    initExtra = ''
      xset r rate 200 100
    '';
  };


}

# { config, lib, pkgs, ... }:
# {
#   xsession.windowManager.xmonad = {
#     enable = true;
#     enableContribAndExtras = true;
#     config =./xmonad.hs;
#   };
# }
