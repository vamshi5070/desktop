{ config, lib, pkgs, ... }:
{
  services.xcape = {
    enable = true;
    mapExpression = {Hyper_L="Tab"; Hyper_R = "backslash";};
  };
}
